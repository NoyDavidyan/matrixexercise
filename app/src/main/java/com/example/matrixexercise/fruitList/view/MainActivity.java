package com.example.matrixexercise.fruitList.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrixexercise.R;
import com.example.matrixexercise.fruitDetails.view.FruitDetailsActivity;
import com.example.matrixexercise.fruitList.presenter.IPresenter;
import com.example.matrixexercise.fruitList.presenter.Presenter;
import com.example.matrixexercise.model.Fruit;
import com.example.matrixexercise.model.Model;

import java.util.ArrayList;

/**
 * Created by Noy davidyan on 15/10/2021.
 */


public class MainActivity extends AppCompatActivity implements IView {

    IPresenter presenter;
    RecyclerView fruitsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fruitsRecycler = findViewById(R.id.fruits_recycler);
        fruitsRecycler.setHasFixedSize(true); //For scrolling a fixed size fruits
        fruitsRecycler.setLayoutManager(new LinearLayoutManager(this));

        presenter = new Presenter(this, Model.getModel(this)); // Creates a model once time
        presenter.onStartActivity(this);
    }

    @Override
    public void displaysFruitsList(ArrayList<Fruit> fruits) {

        if (fruits != null) {
            FruitsAdapter fruitsAdapter = new FruitsAdapter(fruits);
            fruitsRecycler.setAdapter(fruitsAdapter);

            //Call back the click on the fruit
            fruitsAdapter.setListener(new FruitsAdapter.FruitListener() {
                @Override
                public void onFruitClicked(String fruitNameClicked_str) {

                    Intent intent = new Intent(MainActivity.this, FruitDetailsActivity.class);
                    intent.putExtra("fruit_name", fruitNameClicked_str);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
