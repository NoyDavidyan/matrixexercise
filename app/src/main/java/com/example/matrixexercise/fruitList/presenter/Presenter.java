package com.example.matrixexercise.fruitList.presenter;

import android.content.Context;

import com.example.matrixexercise.fruitList.view.IView;
import com.example.matrixexercise.model.Fruit;
import com.example.matrixexercise.model.IModel;
import com.example.matrixexercise.model.Model;

import java.util.ArrayList;

/**
 * Created by Noy davidyan on 15/10/2021.
 */


public class Presenter implements IPresenter, IModel.OnFinishedFruitsListener {

    IView iView;
    Model model;


    public Presenter(IView iView, Model model) {
        this.iView = iView;
        this.model = model;
    }

    @Override
    public void onStartActivity(Context context) {
        model.getAllDetailsFruits(this);
    }

    @Override
    public void onDestroy() {
        if (iView != null)
            iView = null;
    }

    @Override
    public void onDisplayFruits(ArrayList<Fruit> fruitArray) {
        iView.displaysFruitsList(fruitArray);
    }
}
