package com.example.matrixexercise.fruitList.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.matrixexercise.R;
import com.example.matrixexercise.model.Fruit;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public class FruitsAdapter extends RecyclerView.Adapter<FruitsAdapter.FruitsViewHolder> {

    private List<Fruit> fruitList;

    private FruitListener listener; // Call back to the activity is called

    public interface FruitListener {
        void onFruitClicked(String fruitName_str);
    }

    public void setListener(FruitListener listener) {
        this.listener = listener;
    }

    public FruitsAdapter(List<Fruit> fruits) {
        this.fruitList = fruits;
    }

    public class FruitsViewHolder extends RecyclerView.ViewHolder {

        ImageView fruitIv;
        TextView fruitNameTv;

        public FruitsViewHolder(View itemView) {
            super(itemView);
            fruitIv = itemView.findViewById(R.id.fruit_image_iv);
            fruitNameTv = itemView.findViewById(R.id.fruit_name_tv);

            //Identify a click on the selected fruit
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null)
                        listener.onFruitClicked(fruitList.get(getAbsoluteAdapterPosition()).getNameStr());
                }
            });
        }
    }


    //Is inflate the cell view and hold references for all fruits cells
    @Override
    public FruitsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fruit_cell, parent, false);
        FruitsViewHolder fruitsViewHolder = new FruitsViewHolder(view);
        return fruitsViewHolder;
    }

    //Is called each time scrolling the list and initializes the fruit details
    @Override
    public void onBindViewHolder(FruitsViewHolder holder, int position) {
        Fruit fruit = fruitList.get(position);

        String url = fruit.getImageUrlStr();
        if (url != null && !url.isEmpty())
            Picasso.get().load(fruit.getImageUrlStr()).into(holder.fruitIv, new Callback() {
                @Override
                public void onSuccess() {
                }

                @Override
                public void onError(Exception e) {
                    Toast.makeText(holder.fruitIv.getContext(), "Error" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        holder.fruitNameTv.setText(fruit.getNameStr());
    }

    //Get the size of the fruits list;
    @Override
    public int getItemCount() {
        return fruitList.size();
    }

}
