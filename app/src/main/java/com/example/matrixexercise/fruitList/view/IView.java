package com.example.matrixexercise.fruitList.view;

import com.example.matrixexercise.model.Fruit;

import java.util.ArrayList;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public interface IView {

    void displaysFruitsList(ArrayList<Fruit> fruitsDetailsArray);
}
