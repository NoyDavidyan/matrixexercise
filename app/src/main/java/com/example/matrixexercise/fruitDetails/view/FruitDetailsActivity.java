package com.example.matrixexercise.fruitDetails.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.matrixexercise.R;
import com.example.matrixexercise.fruitDetails.presenter.IPresenter;
import com.example.matrixexercise.fruitDetails.presenter.Presenter;
import com.example.matrixexercise.model.Fruit;
import com.example.matrixexercise.model.Model;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public class FruitDetailsActivity extends AppCompatActivity implements IView {

    ImageView imageIv;
    TextView nameTv;
    TextView descriptionTv;
    TextView priceTv;

    IPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit_details);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.fruit_details));

        imageIv = findViewById(R.id.image_iv);
        nameTv = findViewById(R.id.name_tv);
        descriptionTv = findViewById(R.id.description_tv);
        priceTv = findViewById(R.id.price_tv);

        if (getIntent() != null) {
            String fruitNameStr = getIntent().getStringExtra("fruit_name");

            if (fruitNameStr != null && !fruitNameStr.isEmpty()) {
                presenter = new Presenter(this, Model.getModel(this));
                presenter.onStartActivity(this, fruitNameStr);
            }
        }
    }

    @Override
    public void displaysFruitDetails(Fruit fruit) {

        if (fruit != null) {

            nameTv.setText(fruit.getNameStr());
            descriptionTv.setText(fruit.getDescriptionStr());
            priceTv.setText(fruit.getPriceInt() + "");

            String url = fruit.getImageUrlStr();
            if (url != null && !url.isEmpty())

                Picasso.get().load(url).resize(300, 300).transform(new CropCircleTransformation()).into(imageIv,
                        new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError(Exception e) {
                                Toast.makeText(FruitDetailsActivity.this, "Error - " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
        } else {
            Toast.makeText(FruitDetailsActivity.this, getString(R.string.fruit_not_found), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
