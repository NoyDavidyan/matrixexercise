package com.example.matrixexercise.fruitDetails.presenter;

import android.content.Context;

import com.example.matrixexercise.fruitDetails.view.IView;
import com.example.matrixexercise.model.Fruit;
import com.example.matrixexercise.model.IModel;
import com.example.matrixexercise.model.Model;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public class Presenter implements IPresenter, IModel.OnFinishedDetailsFruitListener {

    IView iView;
    Model model;

    public Presenter(IView iView, Model model) {
        this.iView = iView;
        this.model = model;
    }

    @Override
    public void onStartActivity(Context context, String fruitNameToDisplayStr) {
        model.getFruitDetailsByName(this, fruitNameToDisplayStr);
    }

    @Override
    public void onDestroy() {
        if (iView != null)
            iView = null;
    }

    @Override
    public void onDisplayDetailsFruit(Fruit fruit) {
        iView.displaysFruitDetails(fruit);
    }
}
