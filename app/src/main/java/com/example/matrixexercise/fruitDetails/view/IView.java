package com.example.matrixexercise.fruitDetails.view;

import com.example.matrixexercise.model.Fruit;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public interface IView {

    void displaysFruitDetails(Fruit fruit);
}
