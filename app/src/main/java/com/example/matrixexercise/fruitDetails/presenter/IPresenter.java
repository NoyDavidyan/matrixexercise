package com.example.matrixexercise.fruitDetails.presenter;

import android.content.Context;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public interface IPresenter {

    // Method to be called when the activity created
    void onStartActivity(Context context, String fruitNameToDisplayStr);

    // Method to destroy lifecycle of MainActivity
    void onDestroy();
}
