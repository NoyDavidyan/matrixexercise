package com.example.matrixexercise.model;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public class Fruit {

    private String nameStr;
    private String imageUrlStr;
    private String descriptionStr;
    private int priceInt;

    public Fruit(String nameStr, String imageUrlStr, String descriptionStr, int priceInt) {
        this.nameStr = nameStr;
        this.imageUrlStr = imageUrlStr;
        this.descriptionStr = descriptionStr;
        this.priceInt = priceInt;
    }

    public String getNameStr() {
        return nameStr;
    }

    public void setNameStr(String nameStr) {
        this.nameStr = nameStr;
    }

    public String getImageUrlStr() {
        return imageUrlStr;
    }

    public void setImageUrlStr(String imageUrlStr) {
        this.imageUrlStr = imageUrlStr;
    }

    public String getDescriptionStr() {
        return descriptionStr;
    }

    public void setDescriptionStr(String descriptionStr) {
        this.descriptionStr = descriptionStr;
    }

    public int getPriceInt() {
        return priceInt;
    }

    public void setPriceInt(int priceInt) {
        this.priceInt = priceInt;
    }
}
