package com.example.matrixexercise.model;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Noy davidyan on 15/10/2021.
 */


public class Model implements IModel {

    final private String URL = "https://dev-api.com/fruitsBT/getFruits";
    final private String TAG = "Model";

    // JSON Node names
    private static final String TAG_FRUITS = "fruits";
    private static final String TAG_NAME = "name";
    private static final String TAG_IMAGE_URL = "image";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_PRICE = "price";

    ArrayList<Fruit> fruitsArray = null;
    JSONObject json = null;
    JSONArray jFruitsArray = null;

    // Static variable model_instance of type Model
    private static Model model_instance = null;

    // Constructor of this class, here private constructor is is used to restricted to this class itself
    private Model(Context context) {
        createJsonArray(context);
        createFruitsArrayByJsonOnj(context);
    }

    // Static method to create instance of Singleton class
    public static Model getModel(Context context) {
        if (model_instance == null) {   // To ensure only one instance is created
            model_instance = new Model(context);
        }
        return model_instance;
    }

    public void createJsonArray(Context context) {

        // Creating JSON Parser instance
        JsonParser jParser = new JsonParser();

        try {
            // Getting JSON string from URL
            json = jParser.getJSONFromUrl(context, URL);
            // Getting array of fruits
            if(json != null)
                jFruitsArray = json.getJSONArray(TAG_FRUITS);

        } catch (JSONException e) {
            Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void createFruitsArrayByJsonOnj(Context context) {

        if (jFruitsArray != null && jFruitsArray.length() > 0) {

            try {
                // Looping through all fruits
                for (int i = 0; i < jFruitsArray.length(); i++) {
                    JSONObject fruitJson = jFruitsArray.getJSONObject(i);

                    // Get the fruit data by each json
                    String name = fruitJson.getString(TAG_NAME);
                    String imageUrlStr = fruitJson.getString(TAG_IMAGE_URL);
                    String descriptionStr = fruitJson.getString(TAG_DESCRIPTION);
                    int priceInt = fruitJson.getInt(TAG_PRICE);

                    Fruit newFruit = new Fruit(name, imageUrlStr, descriptionStr, priceInt);

                    if (fruitsArray == null)
                        fruitsArray = new ArrayList<>();

                    fruitsArray.add(newFruit);
                }
            } catch (JSONException e) {
                Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void getAllDetailsFruits(OnFinishedFruitsListener listener) {
        listener.onDisplayFruits(this.fruitsArray);
    }

    @Override
    public void getFruitDetailsByName(OnFinishedDetailsFruitListener listener, String fruitNameToDisplayStr) {

        Fruit fruitToDisplay = null;

        if (fruitsArray != null) {
            for (Fruit fruit : fruitsArray) {
                if (fruit.getNameStr().equals(fruitNameToDisplayStr)) {
                    fruitToDisplay = fruit;
                    break;
                }
            }
        }
        listener.onDisplayDetailsFruit(fruitToDisplay);
    }


 /*   public JSONObject getJSONFromUrl(Context context ,String urlToConnectStr) {

        JSONObject jsonObj = null;

        try {
            String jsonStr = new JsonParserObject.JsonTask(context).execute(urlToConnectStr).get();
            jsonObj = new JSONObject(jsonStr); // Parse the string to a JSON object

        } catch (ExecutionException e) {
            Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (InterruptedException e) {
            Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (JSONException e) {
            Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        return jsonObj;
    }

    private class JsonTask extends AsyncTask<String, String, String> {

        Context context;
        ProgressDialog pd;

        JsonTask(Context context){
            this.context = context;
        }
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(context);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;
            StringBuffer buffer = null;
            String line = "";

            try {
                java.net.URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                buffer = new StringBuffer();

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                }

            } catch (MalformedURLException e) {
                Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();

            } catch (IOException e) {
                Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            return  buffer.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pd.isShowing()){
                pd.dismiss();
            }
        }
    }*/
}
