package com.example.matrixexercise.model;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by Noy davidyan on 15/10/2021.
 */


class JsonParser {

    // Constructor
    public JsonParser() { }

    public JSONObject getJSONFromUrl(Context context ,String urlToConnectStr) {

        JSONObject jsonObj = null;

        try {
            String jsonStr = new JsonTask(context).execute(urlToConnectStr).get();
             jsonObj = new JSONObject(jsonStr); // Parse the string to a JSON object

        } catch (ExecutionException e) {
            Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (InterruptedException e) {
            Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (JSONException e) {
            Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        return jsonObj;
    }

    private class JsonTask extends AsyncTask<String, String, String> {

        Context context;
        ProgressDialog pd;

        JsonTask(Context context){
            this.context = context;
        }
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(context);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;
            StringBuffer buffer = null;
            String line = "";

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                buffer = new StringBuffer();

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                }

            } catch (MalformedURLException e) {
                Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();

            } catch (IOException e) {
                Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    Toast.makeText(context, "Error - "+e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            return  buffer.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pd.isShowing()){
                pd.dismiss();
            }
        }
    }

}
