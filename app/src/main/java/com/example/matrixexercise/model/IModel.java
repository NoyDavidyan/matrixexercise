package com.example.matrixexercise.model;

import java.util.ArrayList;

/**
 * Created by Noy davidyan on 15/10/2021.
 */

public interface IModel {

    // nested interface to be
    interface OnFinishedFruitsListener {
        // function to be called once the Handler of Model it's execution
        void onDisplayFruits(ArrayList<Fruit> fruitArray);
    }

    // nested interface to be
    interface OnFinishedDetailsFruitListener {
        // function to be called once the Handler of Model it's execution
        void onDisplayDetailsFruit(Fruit fruit);
    }

    void getAllDetailsFruits(OnFinishedFruitsListener listener);

    void getFruitDetailsByName(OnFinishedDetailsFruitListener listener, String fruitNameToDisplayStr);
}
